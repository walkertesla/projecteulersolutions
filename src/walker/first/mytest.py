
"""
Problem 20
"""
from __builtin__ import False
from Carbon.Aliases import false
def factorial(n):
    if n==1:
        return 1
    else:
        return n*factorial(n-1)

def sum_of_num(x):
    if x<10:
        return x
    else:
        return (x%10+sum_of_num((x-x%10)/10))

"""print sum_of_num(factorial(100))"""

"""
Problem 21
"""
def sum_of_list(apple):
    total=0
    for x in apple:
        total+=x
    return total
def list_factors(x):
    banana=[]
    for a in range(1,x):
        if x%a==0:
            banana.append(a)
    return banana
def sum_proper_factors(z):
    return sum_of_list(list_factors(z))
def amicable_checker(y):
    total=0
    for x in range(1,y):
        if x==sum_proper_factors(sum_proper_factors(x)) and (x!= sum_proper_factors(x)):
            total=total+x
    return total

"""print amicable_checker(10001)"""

"""
Problem 22
"""
def num_of_ways(x):
    if x<0:
        return 0
    elif x==0:
        return 1
    elif x==1:
        return 1
    elif x==2:
        return 2
    elif x==3:
        return 3
    else:
        return num_of_ways(x-200)+num_of_ways(x-100)+num_of_ways(x-50)+num_of_ways(x-20)+num_of_ways(x-10)+num_of_ways(x-5)+num_of_ways(x-2)+num_of_ways(x-1)

"""print num_of_ways(200)"""

"""
Problem 33
"""

def checker(y):
    cat=[]
    for a in range(10,y):
        for b in range(10,a):
            if a%10 != 0 and b%10 != 0:
                if b/a==((b-b%10)/10)/(a%10):
                    cat.append(float(b/a))
    return cat

"""print checker(100)"""

"""
Problem 48
"""

def power_of_y(y):
    return (y**y)%10000000000
def sumn(n):
    total=0
    for x in range(1,n+1):
        total=total+power_of_y(x)
    return total
"""print sumn(1000)%10000000000"""

"""
Problem 39
"""
import math
def half_of_factors(z):
    grapes=[]
    for x in range (1,int(math.floor(math.sqrt(z)+1))):
        if z%x==0:
            grapes.append(x)
    return grapes


def number_solutions(y):
    total=0
    for x in half_of_factors(y):
        if (y/x - x > 0) and (x > (y/x-x)):
            total+=1
    return total

def high_solutions(m):
    my_list=[]
    for x in range(1,m+1):
        my_list.append(number_solutions(x))
    index_of_maximum = 0
    for y in range (1,m):
        if my_list[y]>my_list[index_of_maximum]:
            index_of_maximum=y
    return index_of_maximum+1

"""print high_solutions(1000)"""

"""
Problem 97
"""

"""print (28433*(2**7830457)+1)%10000000000"""

"""
Problem 12
"""
"""import math"""

def num_factors(x):
    total=0
    for a in range(1,int(math.floor(math.sqrt(x)+1))):
        if x%a == 0:
            total+=1
    total*=2
    if math.floor(math.sqrt(x))==math.ceil(math.sqrt(x)):
        total-=1
    return total

def smallest_triangle(z):
    counter=1
    while num_factors(counter*(counter+1)/2) < z:
        counter+=1
    else:
        return (counter+1)*counter/2
    
"""print smallest_triangle(500)"""

"""
Problem 29
"""

def distinct_powers(z):
    the_list=[]
    for a in range(2,z+1):
        for b in range(2,z+1):
            for x in the_list:
                if x==a**b:
                    break
            else:
                the_list.append(a**b)
    return len(the_list)

"""print distinct_powers(100)"""

"""
Problem 46
"""
def is_prime(x):
    if x==2 or x==3:
        return True
    for a in range(2,int(math.floor(math.sqrt(x))+1)):
        if x%a==0:
            return False
    else:
        return True

"""print is_prime(17)"""
def smallest_outlier(a):
    counter=1
    while counter>0:
        if is_prime(2*counter+1)==True:
            counter+=1    
        else: 
            apple=1
            while 2*(apple**2)<(2*counter+1):
                if is_prime(2*counter+1-2*(apple**2))==False:
                    apple+=1
                else:
                    counter+=1
                    break
            else:
                return 2*counter+1
                
"""print smallest_outlier(21)"""

"""
Problem 25
"""

def fibonacci_number(x):
    if x==1 or x==2:
        return 1
    else:
        return fibonacci_number(x-1)+fibonacci_number(x-2)

coefficient = math.log10(1/(math.sqrt(5)))
golden_log = math.log10((1+math.sqrt(5))/2)


def number_of_digits(y):
    a=1
    while coefficient+a*golden_log<(y-1):
        a+=1
    else:
        return a

"""print number_of_digits(1000)"""

"""
Problem 16
See Problem 20 for function.
"""
"""print sum_of_num(2**1000)"""

"""
Problem 206
"""
def square_tester(z):
    a=z**2
    if a%10==0:
        if ((a-a%100)/100)%10==9:
            if ((a-a%10000)/10000)%10==8:
                if ((a-a%1000000)/1000000)%10==7:
                    if ((a-a%(10**8))/(10**8))%10==6:
                        if ((a-a%(10**10))/(10**10))%10==5:
                            if ((a-a%(10**12))/(10**12))%10==4:
                                if ((a-a%(10**14))/(10**14))%10==3:
                                    if ((a-a%(10**16))/(10**16))%10==2:
                                        if ((a-a%(10**18))/(10**18))%10==1:
                                            return True
                                        else:
                                            return False
                                    else:
                                        return False
                                else:
                                    return False
                            else:
                                return False
                        else:
                            return False
                    else:
                        return False
                else:
                    return False
            else:
                return False
        else:
            return False
    else:
        return False

starter=10**9
def smallest_square(x):
    while square_tester(x)==False:
        x+=10
    else:
        return x

"""print smallest_square(starter)"""

"""
Problem 56
See Problem 20 for function
"""

def largest_digit_sum(c):
    my_result=0
    for a in range(1,c):
        for b in range(1,c):
            if sum_of_num(a**b)>my_result:
                my_result=sum_of_num(a**b)
    return my_result

"""print largest_digit_sum(100)"""

"""
Problem 63
"""

def digits_to_power(x):
    a=1
    total=0
    while a**x<(10**x):
        if 10**(x-1)<=a**x:
            total+=1
        a+=1
    else:
        return total

def all_special_nums(z):
    total=0
    for x in range(z):
        total+=digits_to_power(x)
    return total

"""print all_special_nums(100)"""

"""
Problem 47
"""

def number_prime_factors(x):
    the_list=[0]
    while x>1:
        a=2
        while a<=x:
            if x%a==0:
                if a!=the_list[len(the_list)-1]:
                    the_list.append(a)
                x/=a
            else:
                a+=1
    else:
        return len(the_list)-1

def four_factors(x):
    if number_prime_factors(x)==4:
        return True
    else:
        return False

def four_checker(z):
    b=100000
    while b<200000:
        if four_factors(b) and four_factors(b+1) and four_factors(b+2) and four_factors(b+3):
            return b
        else:
            b+=1
    else:
        return "None here"

"""print four_checker(5)"""

"""
Problem 92
"""
def digit_square(x):
    total=0
    while x>=10:
        total+=((x%10)**2)
        x=(x-x%10)/10
    else:
        total+=x**2
    return total

def ends_at_eighty_nine(y):
    
    while y!=1 and y!=89 and y!=145 and y!=42 and y!=20 and y!=4 and y!=16 and y!=37 and y!=58:
        while y%10==0:
            y/=10
        y=digit_square(y)
    else:
        if y==1:
            return False
        else:
            return True

def under_number(z):
    total=0
    for x in range(2,z):
        if ends_at_eighty_nine(x)==True:
            total+=1
    return total

"""print under_number(10000000)"""

"""
Problem 69???
"""
def order_prime_factors(x):
    the_list=[0]
    while x>1:
        a=2
        while a<=x:
            if x%a==0:
                if a!=the_list[len(the_list)-1]:
                    the_list.append(a)
                x/=a
            else:
                a+=1
    else:
        the_list.remove(0)
        return the_list
    
def product_list(a):
    numerator=1.0
    denominator=1.0
    for x in a:
        numerator*=x
        denominator*=(x-1)
    return float(numerator/denominator)

def totient_fraction(a):
    return product_list(order_prime_factors(a))


def maximum_totient_fraction(n):
    max_index=2
    for x in range(2,n+1):
        if totient_fraction(x) > totient_fraction(max_index):
            max_index=x
    return max_index

"""print maximum_totient_fraction(1000000)"""
